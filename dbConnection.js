import mongoose from 'mongoose';

const DBConnect = (uri) => {
    mongoose.connect(uri, { useNewUrlParser: true });
    const connection = mongoose.connection;

    if (!connection) {
        console.log('db not found!');
    } else {
        console.log('db connected!');
    }
}

export default DBConnect;
