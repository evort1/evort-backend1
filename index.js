import express from 'express';
import DBConnect from "./dbConnection";
import indexRoutes from "./src/routes/routes";
import cookieParser from 'cookie-parser';
import logger from 'morgan';
import bodyParser from 'body-parser';
import cors from 'cors';
import morgan from 'morgan';

const app = express()
const port = process.env.PORT || 9000;
const uri = 'mongodb://localhost:27017/evort';

//middleware
//Preventing cors errors.
const corsOptions = {
    origin: "http://localhost:3000"
};

app.use(cors(corsOptions));
app.use(morgan('combined'));
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(express.static('uploads'));

//success listening.
app.listen(port, () => {
    console.log(`app is listening to PORT ${port}`)
});

//routing
indexRoutes(app);

//DB config
DBConnect(uri);

export default app;
