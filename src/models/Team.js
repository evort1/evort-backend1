import mongoose from 'mongoose';
import mongooseUniqueValidator from 'mongoose-unique-validator';

const { Schema } = mongoose;

const TeamSchema = new Schema ({
  name: {
    type: String,
    required: true,
    unique: true
  },
  description: {
    type: String
  },
  image:{
    type: String,
  },
  candidates: [
    {
      type: [Schema.Types.ObjectId],
      ref: 'User',
      default: null
    }
  ],
  tournaments: [
    {
      type: [Schema.Types.ObjectId],
      ref: 'Tournament',
      default: null
    }
  ],
  pendingUser: [
    {
      type: [Schema.Types.ObjectId],
      ref: 'User',
      default: null
    }
  ]
}, { timestamps: { createdAt: 'created_at' } });

TeamSchema.plugin(mongooseUniqueValidator);

const Team = mongoose.model('Team', TeamSchema, 'Teams');

export default Team;