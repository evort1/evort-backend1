import mongoose from 'mongoose';
import mongooseUniqueValidator from 'mongoose-unique-validator';

const { Schema } = mongoose;

const UserSchema = new Schema({
	username: {
		type: String,
		required: true
	},
	email: {
		type: String,
		unique: true,
		required: true
	},
	password: {
		type: String,
		required: true
	},
	name: {
		type: String,
		required: true
	},
	tournaments: [
		{
			type: Schema.Types.ObjectId, ref: 'Tournament',
			default: null
		}
	],
	teams: [
		{
			type: Schema.Types.ObjectId, ref: 'Team',
			default: null
		}
	],
  points: {
	MobileLegendBangBang: {
		type: Number,
	default: 0
	},
	DefenceOfTheAncient2: {
		type: Number,
	default: 0
	}
}
}, { timestamp: true });

UserSchema.plugin(mongooseUniqueValidator);

const User = mongoose.model('Users', UserSchema, 'users');

export default User;