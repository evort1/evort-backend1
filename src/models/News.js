import mongoose from 'mongoose';
import mongooseUniqueValidator from 'mongoose-unique-validator';

const { Schema } = mongoose;

const NewsSchema = new Schema({
  title: {
    type: String,
  },
  body: {
    type: String
  },
  image: {
    type: String,
  }
}, { timestamps: { createdAt: 'created_at' } });

NewsSchema.plugin(mongooseUniqueValidator);

const News = mongoose.model('News', NewsSchema, 'News');

export default News;
