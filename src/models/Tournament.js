import mongoose from 'mongoose';
import mongooseUniqueValidator from 'mongoose-unique-validator';

const { Schema } = mongoose;

const TournamentSchema = new Schema ({
	name: {
		type: String,
		required: true,
		unique: true
	},
	description: {
		type: String,
		required: true
	},
	image:{
		type: String,
		default: null
	},
	game: {
		type: String,
		required: true
	},
	startDate: {
		type: Date,
		required: true
	},
	endDate: {
		type: Date,
		required: true
	},
	prizePool: {
		type: Number,
		required: true
	},
	entryFee: {
		type: Number,
		required: true
	},
	slotSize: {
		type: Number,
		required: true
	},
	rules: {
		type: String
	},
	url: {
		type: String,
		default: ''
	},
	teams: [
		{
			type: [Schema.Types.ObjectId],
			ref: 'Team',
			default: null
		}
	],
	bracketSeeds: [
		{
			type: "String",
			default: null
		}
	],
	dateSeeds: [
		{
			type: "String",
			default: null
		}
	],
	status: {
		type: String,
		default: 'upcoming'
	},
	createdBy: {
		type: Schema.Types.ObjectId,
		ref: 'User',
		default: null
	},
	winner: {
		type: String,
		default: null
	},
	pendingTeam: [
		{
			type: [Schema.Types.ObjectId],
			ref: 'Team',
			default: null
		}
	]
}, { timestamps: { createdAt: 'created_at' } });

TournamentSchema.plugin(mongooseUniqueValidator);

const Tournament = mongoose.model('Tournaments', TournamentSchema, 'Tournaments');

export default Tournament;