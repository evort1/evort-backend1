import UserService from "../services/UserService";

const { getById, getByArrayOfId, updateUserPoints, getLeaderboard } = UserService;

class UserController {

  static fetchById = async (req, res, next) => {
    try {
      const { params: { userId } } = req;
      const singleUserId = await getById(userId);
      await res.status(200);
      await res.json(singleUserId);
    } catch (error) {
      next(error);
    }
  }

  static fetchByMultipleIds = async (req, res, next) => {
    try {
      const { body } = req;
      const users = await getByArrayOfId(body);
      await res.status(200);
      await res.json(users);
    } catch (error) {
      next(error);
    }
  }

  static fetchLeaderboard = async (req, res, next) => {
    try {
      const { params: { number, game } } = req;
      const teams = await getLeaderboard(number, game);
      await res.status(200);
      await res.json(teams);
    } catch (error) {
      next(error);
    }
  }

  static updateUserPoints = async (req, res, next) => {
    try {
      const { body } = req;
      const response = await updateUserPoints(body);
      await res.status(200);
      await res.json(response);
    } catch (error) {
      next(error);
    }
  }
}

export default UserController;