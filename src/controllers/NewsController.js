import NewsService from "../services/NewsService";
import BaseUrl from "../constants/BaseUrl";

const { BASEURL } = BaseUrl;
const { createNews, getAll, getById, getByTitle, deleteByNewsId, getByPagination } = NewsService;

class NewsController {
  static createNews = async (req, res, next) => {
    const { body, file } = req;
    //kalo mau deploy, ubah disini.
    const url = `${req.protocol}://${BASEURL}${req.file.filename}`

    try {
      const response = await createNews(body, file, url);
      await res.status(response.status);
      await res.send(response);
    } catch (error) {
      next(error);
    }
  }

  static deleteByNewsId = async (req, res, next) => {
    try{
      const {params: {newsId}} = req;
      const {status, message} = await deleteByNewsId(newsId);
      await res.status(status);
      await res.json(message);
    }catch (error) {
      next(error);
    }
  }

  static fetchAll = async (req, res, next) => {
    try {
      const news = await getAll();
      await res.status(200);
      await res.json(news);
    } catch (error) {
      next(error);
    }
  }

  static fetchByPagination = async (req, res, next) => {
    try {
      const {params: { page, skip }} = req;
      const news = await getByPagination( page, skip );
      await res.status(200);
      await res.json(news);
    } catch (error) {
      next(error);
    }
  }

  static fetchById = async (req, res, next) => {
    try {
      const { params: { newsId } } = req;
      const singleNewsId = await getById(newsId);
      await res.status(200);
      await res.json(singleNewsId);
    } catch (error) {
      next(error);
    }
  }

  static fetchByTitle = async (req, res, next) => {
    try {
      const { params: { title } } = req;
      const singleNews = await getByTitle(title);
      await res.status(200);
      await res.json(singleNews);
    } catch (error) {
      next(error);
    }
  }
}
export default NewsController;
