import TournamentService from "../services/TournamentService";
import BaseUrl from "../constants/BaseUrl";

const { BASEURL } = BaseUrl;

const {
  createTournament, getAll,
  getById, joinTournament,
  updateTournament, updateTournamentStatus,
  leaveTournament, getByUserId, getByArrayOfId,
  getCarouselTournamentData, getByPaginationAndNameAndFilter,
  deleteByTournamentId, requestJoinTournament,
  removePendingTeam
} = TournamentService;

class TournamentController {
  static createTournament = async (req, res, next) => {
    const {body, file} = req;

    //kalo mau deploy, ubah disini.
    const url = `${req.protocol}://${BASEURL}${req.file.filename}`
    try {
      const response = await createTournament(body, file, url);
      await res
        .status(response.status)
        .send(response);
    } catch (error) {
      next(error);
    }
  }

  static fetchAll = async (req, res, next) => {
    try {
      const tournaments = await getAll();
      await res.status(200);
      await res.json(tournaments);
    } catch (error) {
      next(error);
    }
  }

  static deleteByTournamentId = async (req, res, next) => {
		try{
		  const {params: {tournamentId}} = req;
		  const {status, message} = await deleteByTournamentId(tournamentId);
		  await res.status(status);
		  await res.json(message);
    }catch (error) {
      next(error);
    }
  }

  static fetchByUserId = async (req, res, next) => {
    try {
      const {params: {userId}} = req;
      const tournaments = await getByUserId(userId);
      await res.status(200);
      await res.json(tournaments);
    } catch (error) {
      next(error);
    }
  }

  static fetchById = async (req, res, next) => {
    try {
      const {params: {tournamentId}} = req;
      const singleTournamentId = await getById(tournamentId);
      await res.status(200);
      await res.json(singleTournamentId);
    } catch (error) {
      next(error);
    }
  }

  static fetchByMultipleIds = async (req, res, next) => {
    try {
      const {body} = req;
      const tournaments = await getByArrayOfId(body);
      await res.status(200);
      await res.json(tournaments);
    } catch (error) {
      next(error);
    }
  }

  static joinTournament = async (req, res, next) => {
    try {
      const { body:[{tournamentId }, {userId}, {teamIds}] } = req;
      const response = await joinTournament(tournamentId, teamIds, userId);
      await res.status(200);
      await res.json(response);
    } catch (error) {
      next(error);
    }
  }

  static leaveTournament = async (req, res, next) => {
    try {
      const {params: {tournamentId, teamId, userId}} = req;
      const response = await leaveTournament(tournamentId, teamId, userId);
      await res.status(200);
      await res.json(response);
    } catch (error) {
      next(error);
    }
  }

  static updateTournament = async (req, res, next) => {
    const {params: {tournamentId}, body, file} = req;

    try {
      const {message} = await updateTournament(tournamentId, body, file);
      await res
        .status(200)
        .json({
          message
        });
    } catch (error) {
      next(error);
    }
  }

  static updateTournamentStatus = async (req, res, next) => {
    const {params: {tournamentId, status}} = req;

    try {
      const {message} = await updateTournamentStatus(tournamentId, status);
      await res
        .status(200)
        .json({
          message
        });
    } catch (error) {
      next(error);
    }
  }

  static fetchCarouselTournamentData = async (req, res, next) => {
    try {
      const tournaments = await getCarouselTournamentData();
      await res.status(200);
      await res.json(tournaments);
    } catch (error) {
      next(error);
    }
  }

  static fetchByPaginationAndNameAndFilter = async (req, res, next) => {
    const {params: { page, skip }, query: {name, status, game}} = req;

    try {
      const tournament = await getByPaginationAndNameAndFilter(page, skip, name, status, game);
      res.status(200).send(tournament);
    } catch (error) {
      next(error);
    }
  }

  static requestJoinTournament = async (req, res, next) => {

    const { params: {tournamentId, teamId} } = req;
    try {
      const result = await requestJoinTournament(tournamentId, teamId);
      await res.status(200);
      await res.json(result);
    }catch (error) {
      next(error);
    }
  }

  static removePendingTeam = async (req, res, next) => {
    const { body:[{tournamentId}, {teamIds}] } = req;
    try {
      const result = await removePendingTeam(tournamentId, teamIds);
      await res.status(200);
      await res.json(result);
    }catch (error) {
      next(error);
    }
  }
}

export default TournamentController;
