import AuthService from "../../services/auth/AuthService";

const { createUser, signIn } = AuthService;

class AuthController {
  static register = async (req, res, next) => {
    const { body } = req;
    try {
      const response = await createUser(body);
      res.status(response.status);
      res.json({ message: response.message });
    } catch (error) {
      next(error);
    }
  }

  static login = async (req, res, next) => {
    const { body } = req;
    try {
      const response = await signIn(body);
      res.status(response.status);
      res.send(response);
    } catch (error) {
      next(error);
    }
  }

  static logout = (req, res, next) => {
    try {
      res.clearCookie('token')
      res.status(200);
      res.json({ message: "LogOut..." });
    } catch (error) {
      next(error);
    }
  }
}

export default AuthController;
