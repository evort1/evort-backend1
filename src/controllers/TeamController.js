import TeamService from "../services/TeamService";
import BaseUrl from "../constants/BaseUrl";

const { BASEURL } = BaseUrl;

const {
  joinTeam, createTeam, getAll, getById, getByArrayOfId,
  getByName, leaveTeam, getByPaginationAndSearchByName, requestJoinTeam,
  removePendingUser, deleteByTeamId
  } = TeamService;

class TeamController {
  static createTeam = async (req, res, next) => {
    const { body, file } = req;

    //kalo mau deploy, ubah disini.
    const url = `${req.protocol}://${BASEURL}${req.file.filename}`

    try {
      const response = await createTeam(body, file, url);
      await res.status(response.status);
      await res.send(response);
    } catch (error) {
      next(error);
    }
  }

  static fetchAll = async (req, res, next) => {
    try {
      const teams = await getAll();
      await res.status(200);
      await res.json(teams);
    } catch (error) {
      next(error);
    }
  }

  static deleteByTeamId = async (req, res, next) => {
    try{
      const {params: {teamId}} = req;
      const {status, message} = await deleteByTeamId(teamId);
      await res.status(status);
      await res.json(message);
    }catch (error) {
      next(error);
    }
  }

  static fetchById = async (req, res, next) => {
    try {
      const { params: { teamId } } = req;
      const singleTeamId = await getById(teamId);
      await res.status(200);
      await res.json(singleTeamId);
    } catch (error) {
      next(error);
    }
  }

  static fetchByMultipleIds = async (req, res, next) => {
    try {
      const { body } = req;
      const teams = await getByArrayOfId(body);
      await res.status(200);
      await res.json(teams);
    } catch (error) {
      next(error);
    }
  }

  static fetchByName = async (req, res, next) => {
    try {
      const { params: { teamName } } = req;
      const singleTeamName = await getByName(teamName);
      await res.status(200);
      await res.json(singleTeamName);
    } catch (error) {
      next(error);
    }
  }

  static joinTeam = async (req, res, next) => {
    try {
      const { body:[{teamId }, {userIds}] } = req;
      const response = await joinTeam(teamId, userIds);
      await res.status(200);
      await res.json(response);
    } catch (error) {
      next(error);
    }
  }

  static leaveTeam = async (req, res, next) => {
    try {
      const { params: { teamId, userId } } = req;
      const response = await leaveTeam(teamId, userId);
      await res.status(200);
      await res.json(response);
    } catch (error) {
      next(error);
    }
  }

  static fetchByPaginationAndSearchByName = async (req, res, next) => {
    const { params: { page, skip }, query: {name} } = req;

    try {
      const team = await getByPaginationAndSearchByName(page, skip, name);
      res.status(200).send(team);
    } catch (error) {
      next(error);
    }
  }

  static requestJoinTeam = async (req, res, next) => {
    const { params:{ teamId , userId } } = req;
    try {
      const result = await requestJoinTeam(teamId, userId);
      await res.status(200);
      await res.json(result);
    }catch (error) {
      next(error);
    }
  }

  static removePendingUser = async (req, res, next) => {
    const { body:[{teamId}, {userIds}] } = req;
    try {
      const result = await removePendingUser(teamId, userIds);
      await res.status(200);
      await res.json(result);
    }catch (error) {
      next(error);
    }
  }
}
export default TeamController;
