import News from '../models/News';

class NewsRepository {
  static create = async (NewsData) => {
    return await News.create(NewsData);
  }

  static findAll = async () => {
    return await News.find().sort({ "created_at": -1 });
  }

  static findByPagination = async (page, skip) => {
    return await News.find().sort({ "created_at": -1 }).skip(Math.abs(parseInt((page -1) * skip))).limit(parseInt(skip));
  }

  static findOne = async (teamId) => {
    return await News.findById(teamId);
  }

  static findOneByTitle = async (title) => {
    return await News.findOne({ title: title });
  }

  static deleteById = async (newsId) => {
    return await News.deleteOne({"_id": newsId});
  }

}
export default NewsRepository;