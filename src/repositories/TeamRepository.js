import Team from '../models/Team';

class TeamRepository {

  static findTeamByName = async (teamName) => {
    return await Team.findOne({ name: teamName });
  }

  static deleteById = async (teamId) => {
    return await Team.deleteOne({ "_id": teamId });
  }

  static create = async (teamData) => {
    return await Team.create(teamData);
  }

  static joinTeamById = async (teamId, userId, opts) => {
    return await Team.updateOne(
      { _id: teamId },
      {
        $push: {
          candidates: userId
        },
        opts
      }
    )
  }

  static leaveTeamById = async (teamId, userId, opts) => {
    return await Team.updateOne(
      { _id: teamId },
      {
        $pull: {
          candidates: userId
        },
        opts
      }
    )
  }

  static updateTeamLeaveTournament = async (teamId, tournamentId, opts) => {
    return await Team.updateOne(
      { _id: teamId },
      {
        $pull: {
          tournaments: tournamentId
        },
        opts
      }
    )
  }

  static findOne = async (teamId) => {
    return await Team.findById(teamId);
  }

  static findAll = async () => {
    return await Team.find().sort({ "created_at": -1 });
  }

  static updateTeamJoinTournament = async (teamId, tournamentId, opts) => {
    return await Team.updateOne(
      { _id: teamId },
      {
        $push: {
          tournaments: tournamentId
        },
        opts
      }
    )
  }

  static findPaginationAndSearchByName = async (page, skip, keyword) => {
    const teams = await Team
      .find({ name: { $regex: new RegExp(`${keyword}`) } })
      .sort({ "created_at": -1 })
      .skip(Math.abs(parseInt((page - 1) * skip)))
      .limit(parseInt(skip));

    const searchByNameTeams = await (await Team.find({ name: { $regex: new RegExp(`${keyword}`) } }));
    const end = Math.ceil(searchByNameTeams.length / skip);

    return {teams, end};
  }

  static updatePendingUserJoinTeam = async (teamId, userId) => {
    return await Team.updateOne(
      {_id: teamId},
      {
        $push: {
          pendingUser: userId
        }
      }
    )
  }

  static updateRemovePendingUserJoinTeam = async (teamId, userId) => {
    return await Team.updateOne(
      {_id: teamId},
      {
        $pull: {
          pendingUser: userId
        }
      }
    )
  }


}
export default TeamRepository;