import User from '../models/User';

class UserRepository {
	static findByEmail = async (email) => {
		return await User.findOne({ email });
	}

	static findOne = async (userId) => {
		return await User.findById(userId);
	}

	static create = async (user) => {
		return await User.create(user);
	}

	static findByArrayOfId = async (userIds) => {
		return await User.find({ "_id": { $in: [...userIds] } });
	}

	static updateUserJoinTeam = async (userId, teamId, opts) => {
		return await User.updateOne(
			{ _id: userId },
			{
				$push: {
					teams: teamId
				},
				opts
			}
		)
	}

	static updateUserLeaveTeam = async (userId, teamId, opts) => {
		return await User.updateOne(
			{ _id: userId },
			{
				$pull: {
					teams: teamId
				},
				opts
			}
		)
	}

	static updateUserJoinTournament = async (userId, tournamentId, opts) => {
		return await User.updateOne(
			{ _id: userId },
			{
				$addToSet: {
					tournaments: tournamentId
				},
				opts
			}
		)
	}

	static updateUserLeaveTournament = async (userId, tournamentId, opts) => {
    return await User.updateOne(
      { _id: userId },
      {
        $pull: {
          tournaments: tournamentId
        },
        opts
      }
    )
	}

	static findLeaderboard = async (number, game) => {
		if (game === "DefenceOfTheAncient2") {
			return await User.find().sort({ "points.DefenceOfTheAncient2": -1 }).limit(parseInt(number));
		} else if (game === "MobileLegendBangBang") {
			return await User.find().sort({ "points.MobileLegendBangBang": -1 }).limit(parseInt(number));
		}
	}

	static findIDAndUpdateUserPoint = async (userId, newPoints) => {
		return await User.updateOne(
			{ _id: userId },
			{
				$set: {
					points: newPoints
				}
			}
		)
	}
}
export default UserRepository;