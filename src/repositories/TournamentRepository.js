import Tournament from '../models/Tournament';

class TournamentRepository {
	static create = async (tournament) => {
		return await Tournament.create(tournament);
	}

	static deleteById = async (tournamentId) => {
		return await Tournament.deleteOne({"_id": tournamentId});
	}

	static findAll = async () => {
		return await Tournament.find();
	}

	static findOne = async (tournamentId) => {
		return await Tournament.findById(tournamentId);
	}

	static findByUserId = async (userId) => {
    return await Tournament.find({ "createdBy": userId });
  }

	static findByArrayOfId = async (tournamentIds) => {
    return await Tournament.find({ "_id": { $in: [...tournamentIds] } });
  }

	static joinTournamentById = async (tournamentId, teamId, opts) => {
		return await Tournament.updateOne(
			{ _id: tournamentId },
			{
				$push: {
					teams: teamId
				},
				opts
			}
		)
	}

	static leaveTournamentById = async (tournamentId, teamId, opts) => {
		return await Tournament.updateOne(
			{ _id: tournamentId },
			{
				$pull: {
					teams: teamId
				},
				opts
			}
		)
	}

	static findIDAndUpdateTournament = async (tournamentId, newTournament) => {
		return await Tournament.updateOne(
			{ _id: tournamentId }, {
			$set: newTournament
		})
	}

	static findIDAndUpdateTournamentStatus = async (tournamentId, status) => {
		const payload = {status};

		return await Tournament.updateOne(
			{ _id: tournamentId }, {
			$set: payload
		})
	}

	static getCarouselTournamentData = async () => {
		return await Tournament.find().sort({ prizePool: -1}).limit(10);
	}

	static findByPaginationAndNameAndFilter = async (page, skip, keyword, tournamentStatus, game) => {
		if(game == 'all'){
			game = ['Mobile Legend Bang Bang', 'Defence of the Ancient 2'];
		}else{
			game = [game];
		}

		if(tournamentStatus == 'all'){
			tournamentStatus = ['upcoming', 'ongoing', 'completed'];
		}else{
			tournamentStatus = [tournamentStatus];
		}

		const tournaments = await Tournament.find({
			$and: [
				{ name: { $regex: new RegExp(`${keyword}`) } },
				{ status: { $in: [...tournamentStatus] } },
				{ game: { $in: [...game] } }
				]
			}
		).sort({ "created_at": -1 })
		.skip(Math.abs(parseInt((page - 1) * skip)))
		.limit(parseInt(skip));

		const nameAndFilterTournaments = await Tournament.find({
			$and: [
				{ name: { $regex: new RegExp(`${keyword}`) } },
				{ status: { $in: [...tournamentStatus] } },
				{ game: { $in: [...game] } }
				]
			}
		)
		const end = Math.ceil(nameAndFilterTournaments.length / skip);

    return {tournaments, end};
 }

	static findByName = async (keyword) => {
		return await Tournament.find({ name: {$regex: new RegExp(`${keyword}`) } });
	}

	static updatePendingTeamJoinTournament = async (tournamentId, teamId) => {
		return await Tournament.updateOne(
			{_id: tournamentId},
			{
				$push: {
					pendingTeam: teamId
				}
			}
		)
	}

	static updateRemovePendingTeamJoinTournament = async (tournamentId, teamId) => {
		return await Tournament.updateOne(
			{_id: tournamentId},
			{
				$pull: {
					pendingTeam: teamId
				}
			}
		)
	}
}
export default TournamentRepository;
