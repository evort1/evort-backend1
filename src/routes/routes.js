import express from 'express';
import AuthRouter from "./auth/AuthRoute";
import TournamentRouter from "./TournamentRoute";
import TeamRouter from "./TeamRoute";
import UserRouter from "./UserRoute";
import NewsRoute from "./NewsRoute";

//if you want to make another route, just make a file and import it here.

const router = express.Router();

const indexRoutes = (app) => {
	app.use('/', router);
	router.use('/Auth', AuthRouter);
	router.use('/Tournaments', TournamentRouter);
	router.use('/Teams', TeamRouter);
	router.use('/User', UserRouter);
	router.use('/News', NewsRoute);
}

export default indexRoutes;