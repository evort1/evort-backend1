import express from "express";
import UserController from "../controllers/UserController";

const UserRouter = express.Router();

const { fetchById, fetchByMultipleIds, updateUserPoints, fetchLeaderboard } = UserController;

UserRouter.get('/:userId', fetchById);
UserRouter.post('/usersId-array', fetchByMultipleIds);
UserRouter.patch('/updateUserPoints', updateUserPoints);
UserRouter.get('/leaderboard/:number/:game', fetchLeaderboard);

export default UserRouter