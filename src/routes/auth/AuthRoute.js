import express from 'express';
import AuthController from "../../controllers/auth/AuthController";
// TODO:
// import withAuth from "../../middlewares/AuthMiddleware"; di pake buat check token dari front-end

const AuthRouter = express.Router();

const { register, login, logout} = AuthController;

AuthRouter.post('/register', register);
AuthRouter.post('/login', login);
AuthRouter.post('/logout', logout);

export default AuthRouter;