import express from 'express';
import TeamController from "../controllers/TeamController";
import multer from "multer";
import fs from "fs";
import path from "path";

const TeamRouter = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    const path = "./uploads";
    fs.mkdirSync(path, { recursive: true });
    cb(null, path);
  },
  filename: (req, file, cb) => {
    cb(null, `${file.fieldname}_${+new Date()}${path.extname(file.originalname)}`);
  }
});

const upload = multer({ storage });

const {
  joinTeam, createTeam, fetchById, fetchByMultipleIds,
  fetchAll, fetchByName, leaveTeam, fetchByPaginationAndSearchByName,
  requestJoinTeam, removePendingUser, deleteByTeamId
} = TeamController;

TeamRouter.post('/create', upload.single('image'), createTeam);
TeamRouter.post('/teamId-array', fetchByMultipleIds);
TeamRouter.patch('/:teamId/join', joinTeam);
TeamRouter.get('/:teamId', fetchById);
TeamRouter.get('/', fetchAll);
TeamRouter.get('/name/:teamName', fetchByName);
TeamRouter.patch('/:teamId/leave/:userId', leaveTeam);
TeamRouter.get('/pagination/:page/:skip/search/by', fetchByPaginationAndSearchByName);
TeamRouter.delete('/delete/:teamId', deleteByTeamId);
TeamRouter.patch('/:teamId/request/join/:userId', requestJoinTeam);
TeamRouter.patch('/:teamId/request/join/remove/pendingUser', removePendingUser);

export default TeamRouter;
