import express from 'express';
import NewsController from "../controllers/NewsController";
import multer from "multer";
import fs from "fs";
import path from "path";

const NewsRouter = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    const path = "./uploads";
    fs.mkdirSync(path, { recursive: true });
    cb(null, path);
  },
  filename: (req, file, cb) => {
    cb(null, `${file.fieldname}_${+new Date()}${path.extname(file.originalname)}`);
  }
});

const upload = multer({ storage });

const { createNews, fetchAll, fetchById, fetchByTitle, deleteByNewsId, fetchByPagination} = NewsController;

NewsRouter.post('/create', upload.single('image'), createNews);
NewsRouter.get('/', fetchAll);
NewsRouter.get('/:page/:skip', fetchByPagination);
NewsRouter.get('/:newsId', fetchById);
NewsRouter.get('/title/:title', fetchByTitle);
NewsRouter.delete('/delete/:newsId', deleteByNewsId);


export default NewsRouter;