import express from 'express';
import TournamentController from "../controllers/TournamentController";
import multer from "multer";
import fs from "fs";
import path from "path";

const TournamentRouter = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    const path = "./uploads";
    fs.mkdirSync(path, { recursive: true });
    cb(null, path);
  },
  filename: (req, file, cb) => {
    cb(null, `${file.fieldname}_${+new Date()}${path.extname(file.originalname)}`);
  }
});

const upload = multer({ storage });

const {
  createTournament, fetchAll,
  fetchById, joinTournament,
  updateTournament, updateTournamentStatus,
  leaveTournament, fetchByUserId, fetchByMultipleIds,
  fetchCarouselTournamentData, fetchByPaginationAndNameAndFilter,
  deleteByTournamentId, requestJoinTournament,
  removePendingTeam
} = TournamentController;

TournamentRouter.post('/create', upload.single('image'), createTournament);
TournamentRouter.post('/tournamentsId-array', fetchByMultipleIds);
TournamentRouter.get('/', fetchAll);
TournamentRouter.get('/carousel', fetchCarouselTournamentData);
TournamentRouter.get(`/:tournamentId`, fetchById);
TournamentRouter.get('/users/:userId', fetchByUserId);
TournamentRouter.get('/pagination/:page/:skip/search/by', fetchByPaginationAndNameAndFilter);
TournamentRouter.patch('/:tournamentId/join', joinTournament);
TournamentRouter.patch('/:tournamentId/leave/:teamId/:userId', leaveTournament);
TournamentRouter.patch('/:tournamentId/edit', upload.single('image'), updateTournament);
TournamentRouter.patch('/:tournamentId/edit/:status', updateTournamentStatus);
TournamentRouter.delete('/delete/:tournamentId', deleteByTournamentId);
TournamentRouter.patch('/:tournamentId/request/join/:teamId', requestJoinTournament);
TournamentRouter.patch('/:tournamentId/request/join/remove/pendingTeam', removePendingTeam);

export default TournamentRouter;
