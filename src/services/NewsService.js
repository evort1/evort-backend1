import News from "../models/News";
import fs from 'fs';
import NewsRepository from "../repositories/NewsRepository";
import httpMessageConstants from "../constants/HttpMessageConstants";

const { NEWS_CREATED_SUCCESSFULLY_MESSAGE } = httpMessageConstants;
const { create, findAll, findOne, findOneByTitle, deleteById, findByPagination } = NewsRepository;

class NewsService {
  static createNews = async (body, file, url) => {
    const news = {
      ...body,
      image: file ? url : ''
    }
    const newNews = new News(news);

    try {
      const { id } = await create(newNews);
      return {
        message: NEWS_CREATED_SUCCESSFULLY_MESSAGE,
        status: 201,
        id
      }
    } catch (error) {
      throw error;
    }
  }

  static getByTitle = async (title) => {
    return await findOneByTitle(title);
  }

  static getAll = async () => {
    return await findAll();
  }

  static getByPagination = async (page, skip) => {
    const news = await findByPagination( page, skip);
    
    const AllNews = await findAll();
    const end = Math.ceil(AllNews.length / skip);

    return {news, end};
  }

  static getById = async (teamId) => {
    return await findOne(teamId);
  }

  static deleteByNewsId = async (newsId) => {
    const {ok, deletedCount} =  await deleteById(newsId);

    if(ok != 1) {
      return {
        status: 400,
        message: 'Something went wrong when deleting news!'
      }
    }

    if(ok == 1 || deletedCount == 1) {
      return {
        status: 202,
        message: 'Successfully Delete a News'
      }
    }

  }
}
export default NewsService;
