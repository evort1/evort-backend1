import Tournament from "../models/Tournament";
import TournamentRepository from "../repositories/TournamentRepository";
import TeamRepository from "../repositories/TeamRepository";
import httpStatusConstants from "../constants/HttpStatusConstants";
import httpMessageConstants from "../constants/HttpMessageConstants";
import fs from 'fs';
import UserRepository from "../repositories/UserRepository";
import { isEqual } from 'lodash';

const {
    create, findAll, findOne, joinTournamentById,
    findIDAndUpdateTournament, findIDAndUpdateTournamentStatus,
		leaveTournamentById, findByUserId, findByArrayOfId,
		getCarouselTournamentData, findByPaginationAndNameAndFilter,
	    deleteById, updatePendingTeamJoinTournament,
		updateRemovePendingTeamJoinTournament
    } = TournamentRepository;
const { updateTeamJoinTournament, updateTeamLeaveTournament } = TeamRepository
const { updateUserJoinTournament, updateUserLeaveTournament } = UserRepository
const { HTTP_STATUS_OK, HTTP_STATUS_ERROR } = httpStatusConstants;
const {
    TEAM_EXIST_ERROR, JOIN_TOURNAMENT_FAILED,SUCCESSFULLY_JOINED_TOURNAMENT,
    TOURNAMENT_UPDATED_SUCCESSFULLY_MESSAGE, TOURNAMENT_STATUS_UPDATED_SUCCESSFULLY_MESSAGE,
		LEAVE_TOURNAMENT_FAILED, SUCCESSFULLY_LEAVE_TOURNAMENT, TOURNAMENT_CREATED_SUCCESSFULLY_MESSAGE,
		JOIN_TOURNAMENT_FAILED_SLOT_FULL
    } = httpMessageConstants;
import { isEmpty } from 'lodash';

class TournamentService {
	static createTournament = async (body, file, url) => {

		const tournament = {
			...body,
			image: file ? url : ''
		}
		const newTournament = new Tournament(tournament);

		try {
			const { id } = await create(newTournament);
			return {
        message: TOURNAMENT_CREATED_SUCCESSFULLY_MESSAGE,
        status: 201,
        id
      }
		} catch (error) {
			const { properties } = error.errors.name;
			if (properties.type === 'unique') {
				return {
					message: `${properties.path} already exists`,
					status: 400
				}
			}

			if (properties.type === 'required') {
				return {
					message: `${properties.path} field is required`,
					status: 400
				}
			}
		}
	}

	static getAll = async () => {
		return await findAll();
	}

	static deleteByTournamentId = async (tournamentId) => {

		const tournamentData = await findOne(tournamentId);

		if(!isEmpty(tournamentData.teams)) {
		await Promise.all(
			tournamentData.teams.map(async (teamId) => {
				await TeamRepository.updateTeamLeaveTournament(teamId, tournamentId);
				const teamData = await TeamRepository.findOne(teamId);
				teamData.candidates.map(async (userId) => {
					await UserRepository.updateUserLeaveTournament(userId, tournamentId);
				})
			})
			)
		}

		const {ok, deletedCount} =  await deleteById(tournamentId);

		if(ok != 1) {
			return {
				status: 400,
				message: 'Something went wrong when deleting tournament!'
			}
		}

		if(ok == 1 || deletedCount == 1) {
			return {
				status: 202,
				message: 'Successfully Delete a Tournament'
			}
		}
	}

	static getByUserId = async (tournamentIds) => {
    return await findByUserId(tournamentIds);
	}

	static getById = async (tournamentId) => {
		return await findOne(tournamentId);
	}

	static getByArrayOfId = async (tournamentIds) => {
    return await findByArrayOfId(tournamentIds);
  }

	static joinTournament = async (tournamentId, teamIds, userId, opts) => {
		let response;
		if(isEqual(teamIds.length, 0)) {
			return {
				status: 404,
				message: 'You need to choose a team to be selected'
			}
		}
		if(teamIds.length > 0) {
			response = await Promise.all(
				teamIds.map( async (teamId) => {

					let isAlreadyJoin = false;
					let isTeammateJoinedTournament = false;
					let teammateUserName = '', teamName = '', teammateId = '';
					const tournament = await findOne(tournamentId);
					const teamInformation = await TeamRepository.findOne(teamId);

					if(isEqual(tournament.teams.length, tournament.slotSize)) {
						return {
							status: HTTP_STATUS_ERROR,
							message: JOIN_TOURNAMENT_FAILED_SLOT_FULL
						}
					}

					const user = await UserRepository.findOne(userId);

					await Promise.all(
						user.teams.map(async (userTeamId) => {
							const userTeam = await TeamRepository.findOne(userTeamId);
							userTeam.candidates.map(async (userTeammateId) => {
								await UserRepository.updateUserJoinTournament(userTeammateId, tournamentId);
								const userTeammate = await UserRepository.findOne(userTeammateId);
								userTeammate.tournaments.map((userTeammateTournamentId) => {
									if(userTeammateTournamentId == tournamentId) {
										isTeammateJoinedTournament = true;
										teammateUserName = userTeammate.name;
										teammateId = userTeammate._id;
									}
								})
							})
						})
					)

					await Promise.all(
						tournament.teams.map(async (tournamentTeamId) => {
							const team = await TeamRepository.findOne(tournamentTeamId);
							team.candidates.map((joinedTournamentUserId) => {
								if(isEqual(joinedTournamentUserId, teammateId)) {
									teamName = team.name;
								}
							})
						})
					)

					if(isTeammateJoinedTournament) {
						return {
							status: HTTP_STATUS_ERROR,
							message: `Can't join Tournament. User ${teammateUserName} from your team has joined the tournament with team ${teamName}`
						};
					}

					await tournament.teams.map( (team) => {
						if(team == teamId){
							isAlreadyJoin = true;
						}
					});

					if(isAlreadyJoin) {
						return{
							status: HTTP_STATUS_ERROR,
							message: TEAM_EXIST_ERROR
						};
					}

					try {
						const { nModified: nModifiedJoinTournament } = await joinTournamentById(tournamentId, teamId, userId, opts);
						if(nModifiedJoinTournament < 1) {
							throw new Error(JOIN_TOURNAMENT_FAILED);
						}

						const { nModified: nModifiedUpdateTeamJoinTournament } = await updateTeamJoinTournament(teamId, tournamentId, opts);
						if(nModifiedUpdateTeamJoinTournament < 1) {
							throw new Error(JOIN_TOURNAMENT_FAILED);
						}

						await updateUserJoinTournament(userId, tournamentId, opts);

						return {
							status: HTTP_STATUS_OK,
							message: `Successfully accepting team ${teamInformation.name}`
						}
					} catch (e) {
						throw e;
					}
				})
			)
		}
		return response;
	}

	static leaveTournament = async (tournamentId, teamId, userId, opts) => {
		try {
			const { nModified: nModifiedLeaveTournament } = await leaveTournamentById(tournamentId, teamId, opts);
			if (nModifiedLeaveTournament < 1) {
				throw new Error(LEAVE_TOURNAMENT_FAILED);
			}

			const  { nModified: nModifiedUpdateTeamLeaveTournament } = await updateTeamLeaveTournament(teamId, tournamentId, opts);
			if (nModifiedUpdateTeamLeaveTournament < 1) {
				throw new Error(LEAVE_TOURNAMENT_FAILED);
			}

			const { nModified: nModifiedUpdateUserJoinTournament } = await updateUserLeaveTournament(userId, tournamentId, opts);
			if(nModifiedUpdateUserJoinTournament < 1) {
				throw new Error(JOIN_TOURNAMENT_FAILED);
			}

			return {
				status: HTTP_STATUS_OK,
				message: SUCCESSFULLY_LEAVE_TOURNAMENT
			}
		}
		catch (e) {
			throw e;
		}
	}

	static updateTournament = async (tournamentId, newData, file) => {
		const image = file ? fs.readFileSync(file.path) : null;
		delete newData.image;

		const newTournament = {
			...newData,
			...(file ? {image} : null)
		}

		await findIDAndUpdateTournament(tournamentId, newTournament);

		return {
			message: TOURNAMENT_UPDATED_SUCCESSFULLY_MESSAGE,
			status: 200
		}
	}

	static updateTournamentStatus = async (tournamentId, status) => {
		await findIDAndUpdateTournamentStatus(tournamentId, status);

		return {
			message: TOURNAMENT_STATUS_UPDATED_SUCCESSFULLY_MESSAGE,
			status: 200
		}
	}

	static getCarouselTournamentData = async () => {
		return await getCarouselTournamentData();
	}

	static getByPaginationAndNameAndFilter = async (page, skip, keyword, tournamentStatus, game) => {
		return await findByPaginationAndNameAndFilter(page, skip, keyword, tournamentStatus, game);
	}

	static requestJoinTournament = async (tournamentId, teamId) => {
		const { nModified } = await updatePendingTeamJoinTournament(tournamentId, teamId);
		if(nModified < 1) {
			throw new Error('Request to Join Tournament Failed.')
		}

		return {
			status: HTTP_STATUS_OK,
			message: 'Successfully Requesting to join the Tournament'
		}
	}

	static removePendingTeam = async (tournamentId, teamIds) => {
		let response;
		if(isEqual(teamIds.length, 0)) {
			return {
				status: 404,
				message: 'You need to choose a user to be selected'
			}
		}

		if(teamIds.length > 0) {
			response = await Promise.all(
				teamIds.map( async (teamId) => {
					const { nModified } = await updateRemovePendingTeamJoinTournament(tournamentId, teamId);
					if(nModified < 1) {
						throw new Error( 'Accept Team Failed.');
					}

					return {
						status: HTTP_STATUS_OK,
						message: 'Successfully removing Team'
					}
				})
			)
		}
		return response;
	}
}
export default TournamentService;
