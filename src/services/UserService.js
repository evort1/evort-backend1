import UserRepository from "../repositories/UserRepository";
import Team from "../models/Team";
import User from "../models/User";
import { ObjectId } from 'mongodb';

const { findOne, findByArrayOfId, findIDAndUpdateUserPoint, findLeaderboard } = UserRepository;

class UserService {
  static getById = async (userId) => {
    return await findOne(userId);
  }

  static getByArrayOfId = async (userIds) => {
    return await findByArrayOfId(userIds);
  }

  static updateUserPoints = async ({ participants, winner, game }) => {
    const winnerTeamInformation = await Team.findOne(ObjectId(winner));

    const participantUserIds = []
    await Promise.all(participants.map(async (participant) => {
      const participantInformation = await Team.findOne(ObjectId(participant));
      participantInformation.candidates.map((candidateId) => {
        participantUserIds.push(candidateId);
      })
    }))

    const winnerUserIds = winnerTeamInformation.candidates;

    await Promise.all(participantUserIds.map(async participant => {
      let { points } = await User.findOne(ObjectId(participant));
      points[game] = points[game] + participants.length;

      await findIDAndUpdateUserPoint(participant, points);
    }))

    await Promise.all(winnerUserIds.map(async winnerUserId => {
      let { points } = await User.findOne(ObjectId(winnerUserId));
      points[game] = points[game] +  participants.length * 10;

      await findIDAndUpdateUserPoint(winnerUserId, points);
    }))
  }

  static getLeaderboard = async (number, game) => {
    return await findLeaderboard(number, game);
  }
}

export default UserService;