import UserRepository from "../../repositories/UserRepository";
import User from "../../models/User";
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';

const { findByEmail, create } = UserRepository;

const secret = 'evort'

class AuthService {
	static createToken = (payload) => {
		const token = jwt.sign(payload, secret, {
			expiresIn: '24h'
		});

		return token;
	}

	static createUser = async (user) => {
		if (await findByEmail(user.email)) {
			return {
				message: 'User exists!',
				status: 400
			}
		}

		const newUser = new User(user);
		if (newUser.password) {
			newUser.password = bcrypt.hashSync(newUser.password, 10);
		}
		await create(newUser);

		return {
			message: 'User created!',
			status: 201
		}
	}

	static signIn = async ({ email, password }) => {
		const user = await findByEmail(email);

		if(user){
			const token = this.createToken({email});
			const data = {
				username: user.username,
				email: user.email,
				name: user.name,
				teams: user.teams,
				_id: user._id,
				accessToken: token,
			}

			if (bcrypt.compareSync(password, user.password)) {
				return {
					message: `logged in as ${user.email}`,
					data,
					status: 200,
				}
			}
		}

		return {
			message: 'Wrong credentials, please try again',
			status: 400
		}
	}
}

export default AuthService;
