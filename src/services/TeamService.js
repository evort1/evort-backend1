import Team from "../models/Team";
import TeamRepository from "../repositories/TeamRepository";
import httpStatusConstants from "../constants/HttpStatusConstants";
import httpMessageConstants from "../constants/HttpMessageConstants";
import UserRepository from "../repositories/UserRepository";
import { isEmpty, isEqual } from 'lodash';
import TournamentRepository from "../repositories/TournamentRepository";

const {
  create, findOne, findPaginationAndSearchByName, joinTeamById,
  findAll, findTeamByName, leaveTeamById, updatePendingUserJoinTeam,
  updateRemovePendingUserJoinTeam, deleteById,
  } = TeamRepository;
const { updateUserJoinTeam, updateUserLeaveTeam } = UserRepository
const { HTTP_STATUS_OK, HTTP_STATUS_ERROR } = httpStatusConstants;
const {
  CANDIDATE_EXIST_ERROR, LEAVE_TEAM_FAILED,
  SUCCESSFULLY_LEAVE_TEAM, TEAM_CREATED_SUCCESSFULLY_MESSAGE
} = httpMessageConstants;

class TeamService {
  static createTeam = async (body, file, url) => {
    const team = {
      ...body,
      image: file ? url : ''
    }
    const newTeam = new Team(team);

    try {
      const { id } = await create(newTeam);
      return {
        message: TEAM_CREATED_SUCCESSFULLY_MESSAGE,
        status: 201,
        id
      }
    } catch (error) {
      const { properties } = error.errors.name;
      if (properties.type === 'unique') {
        return {
          message: `${properties.path} already exists`,
          status: 400
        }
      }

      if (properties.type === 'required') {
        return {
          message: `${properties.path} field is required`,
          status: 400
        }
      }
    }
  }

  static deleteByTeamId = async (teamId) => {

    const teamData = await findOne(teamId);
    if(!isEmpty(teamData.tournaments)){
      teamData.tournaments.map(async (joinedTournamentId) => {
        await TournamentRepository.leaveTournamentById(joinedTournamentId, teamId);
      });
    }

    if(!isEmpty(teamData.candidates)){
      teamData.candidates.map(async(userId) => {
        await UserRepository.updateUserLeaveTeam(userId, teamId);
      });
    }

    const {ok, deletedCount} =  await deleteById(teamId);

    if(ok != 1) {
      return {
        status: 400,
        message: 'Something went wrong when deleting team!'
      }
    }

    if(ok == 1 || deletedCount == 1) {
      return {
        status: 202,
        message: 'Successfully Delete a Team'
      }
    }
  }

  static getByName = async (teamName) => {
    return await findTeamByName(teamName);
  }

  static getById = async (teamId) => {
    return await findOne(teamId);
  }

  static getByArrayOfId = async (teamIds) => {
    let teamIdsWithIndex = [];

    teamIds.map((teamId, index) => {
      teamIdsWithIndex.push({teamId, index});
    });

    await Promise.all(
      teamIdsWithIndex.map( async (teamIdWithIndex) => {
        if(teamIdWithIndex.teamId !== "null"){
          const team = await findOne(teamIdWithIndex.teamId);
          teamIdsWithIndex[teamIdWithIndex.index] = team;
        }

        if(teamIdWithIndex.teamId == "null"){
          teamIdsWithIndex[teamIdWithIndex.index] = null;
        }
      })
    )

    return teamIdsWithIndex;
  }

  static getAll = async () => {
    return await findAll();
  }

  static joinTeam = async (teamId, userIds, opts) => {
    let response;
    if(isEqual(userIds.length, 0)) {
      return {
        status: 404,
        message: 'You need to choose a user to be selected'
      }
    }

    if(userIds.length > 0) {
      response = await Promise.all(
        userIds.map( async (userId) => {
        let isAlreadyJoin = false;
        const teamDetail = await findOne(teamId);
        const userDetail = await UserRepository.findOne(userId);

        await teamDetail.candidates.map((candidate) => {
          if (candidate == userId) {
            isAlreadyJoin = true;
          }
        });

        if (isAlreadyJoin) {
          return {
            status: HTTP_STATUS_ERROR,
            message: CANDIDATE_EXIST_ERROR
          };
        }

        try {
          const { nModified: nModifiedJoinTeam } = await joinTeamById(teamId, userId, opts);
          if (nModifiedJoinTeam < 1) {
            throw new Error(`Something went wrong when user ${userDetail.name} is trying to join the team`);
          }
          const { nModified: nModifiedUpdateUserJoinTeam } = await updateUserJoinTeam(userId, teamId, opts);
          if (nModifiedUpdateUserJoinTeam < 1) {
            throw new Error(`Something went wrong when user ${userDetail.name} is trying to join the team`);
          }

          return {
            status: HTTP_STATUS_OK,
            message: `User ${userDetail.name} Successfully joined the team`
          }
        } catch (e) {
          throw e;
        }
      })
      )
    }
    return response;
  }

  static leaveTeam = async (teamId, userId, opts) => {
    try {
      const { nModified: nModifiedLeaveTeam } = await leaveTeamById(teamId, userId, opts);
      if (nModifiedLeaveTeam < 1) {
        throw new Error(LEAVE_TEAM_FAILED);
      }

      const  { nModified: nModifiedUpdateUserLeaveTeam } = await updateUserLeaveTeam(userId, teamId, opts);
      if (nModifiedUpdateUserLeaveTeam < 1) {
        throw new Error(LEAVE_TEAM_FAILED);
      }

      return {
        status: HTTP_STATUS_OK,
        message: SUCCESSFULLY_LEAVE_TEAM
      }
    }
    catch (e) {
      throw e;
    }
  }

  static getByPaginationAndSearchByName = async (page, skip, keyword) => {
    return await findPaginationAndSearchByName(page, skip, keyword);
  }

  static requestJoinTeam = async (teamId, userId) => {
    const { nModified } = await updatePendingUserJoinTeam(teamId, userId);
    if(nModified < 1) {
      throw new Error('Request to Join Team Failed.')
    }

    return {
      status: HTTP_STATUS_OK,
      message: 'Successfully Requesting to join the team'
    }
  }

  static removePendingUser = async (teamId, userIds) => {
    let response;
    if(isEqual(userIds.length, 0)) {
      return {
        status: 404,
        message: 'You need to choose a user to be selected'
      }
    }

    if(userIds.length > 0) {
      response = await Promise.all(
        userIds.map( async (userId) => {
          const { nModified } = await updateRemovePendingUserJoinTeam(teamId, userId);
          if(nModified < 1) {
            throw new Error('Accept User Failed.')
          }

          return {
            status: HTTP_STATUS_OK,
            message: 'Successfully removing user.'
          }
      })
      )
    }
    return response;
  }
}
export default TeamService;
