const httpStatusConstants = {
  HTTP_STATUS_OK: 200,
  HTTP_STATUS_ERROR: 500

};

export default httpStatusConstants;
