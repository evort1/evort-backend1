const httpMessageConstants = {
	TOURNAMENT_CREATED_SUCCESSFULLY_MESSAGE: 'Tournament Created Successfully',
	SUCCESSFULLY_JOINED_TEAM: 'Successfully Joined a Team',
	TEAM_CREATED_SUCCESSFULLY_MESSAGE: 'Successfully Created a Team',
	CANDIDATE_EXIST_ERROR: 'Candidate Already in Team !',
	JOIN_TEAM_FAILED: 'Failed to Join a Team',
	TEAM_EXIST_ERROR: 'Team Already in Tournament !',
	JOIN_TOURNAMENT_FAILED: 'Failed to Join a Tournament',
	SUCCESSFULLY_JOINED_TOURNAMENT: 'Successfully Joined a Tournament',
	LEAVE_TEAM_FAILED: 'Failed to Leave a Team',
	SUCCESSFULLY_LEAVE_TEAM: 'Successfully Leaved a Team',
	TOURNAMENT_UPDATED_SUCCESSFULLY_MESSAGE: 'Tournament Updated Successfully',
	TOURNAMENT_STATUS_UPDATED_SUCCESSFULLY_MESSAGE: 'Tournament Status Updated Successfully',
	LEAVE_TOURNAMENT_FAILED: 'Failed to Leave a Tournament',
	SUCCESSFULLY_LEAVE_TOURNAMENT: 'Successfully Leaved a Tournament',
	NEWS_CREATED_SUCCESSFULLY_MESSAGE: 'News Created Successfully',
	JOIN_TOURNAMENT_FAILED_SLOT_FULL: 'Failed to Join Tournament. Slot full.'
};

export default httpMessageConstants;
